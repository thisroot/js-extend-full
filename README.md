# JavaScript Раширенный курс

ечень рассматриваемых вопросов и методическая информация без примеров кода

1. [ES2015](./ES2015)
    1. [перменные var, let, const и их области видимости](./ES2015/01-var-let-const.md)
    2. [spread | rest - деструкторы массивов](./ES2015/destrcuting-spread-rest.md)
    3. [Функции - стрелочные, контекст](./ES2015/03-functions.md)
    4. [Итераторы](./ES2015/04-iterators.md)
    5. [Генераторы](./ES2015/05-generators.md)
    6. [Классы](./ES2015/06-classes.md)
    7. [Модули](./ES2015/07-modules.md)
    8. [Асинхронное программирование](./ES2015/08-asynchronus-patterns.md)
    9. [Коллекции](./ES2015/09-collections.md)
2. [JS Рецепты](./JS-Recipes)
    1. Webpack
        1. Введение
        2. Плагины
        3. Сервер разработки
        4. Стартовый проект
    2. Библиотеки утилиты
        1. Lodash
            1. each
            2. map
            3. find/filter
            4. without/remove/reject
            5. reduce/every/some
            6. sortBy/groupBy
        7. Ramda
            1. Функциональный стиль
            2. Комопозиции
            3. Проверка полей обьекта
            4. Converge - сравнение результатов функций
            5. Where - фильтрация массива
            6. Условия в рамда
            7. When / Unless
            8. Lenses        
