const EventBus = {
    //хранилище каналов
    channels: {},
    
    subscribe (channelName, listener) {
        //проверяем на наличие зарегистрированного собыьтия
        if (!this.channels[channelName]) {
            this.channels[channelName] = []
        }
        //добавляем коллбек функцию
        this.channels[channelName].push(listener)
    },

    publish (channelName, data) {
        const channel = this.channels[channelName]
        if (!channel || !channel.length) {
            return
        }
        //вызываем событие и передаем в коллбек информацию
        channel.forEach(listener => listener(data))
    }
}

class Order {
    constructor (params) {
        this.params = params
    }

    save () {
        console.log('Order saved')
        EventBus.publish('order/new', {
            userEmail: this.params.userEmail
        })
    }
}

class Mailer {
    constructor () {
        EventBus.subscribe('order/new', this.sendPurchaseEmail)
    }

    sendPurchaseEmail (params) {
        console.log(`Email send to ${params.userEmail}`)
    }
}

const mailer = new Mailer()
const order = new Order({userEmail: 'john@gmail.com'})
order.save()