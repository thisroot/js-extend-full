

const logToConsole = (level, message) => {
    console.log(`${level}:${message}`);
}

const logToConsoleWithDataStamp = (level, message) => {
    var date = new Date();
    console.log(`${date}:${level}:${message}`);
}

  const logToDOMStrategy = (level, message, node) => {
    node.innerHTML = `<div class='${level}'>${message}</div>`
}

  //обработчик
const logger = (strategy, level, message, ...args) => {
    return strategy(level, message, ...args)
}

logger(
    logToConsole,
    'warn',
    'log first message to console'
)

logger(
    logToConsoleWithDataStamp,
    'warn',
    'log second message to console with date'
)