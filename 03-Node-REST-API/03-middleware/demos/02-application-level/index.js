var express = require('express');
var app = express();


//First example
app.use(function (req, res, next) {
  console.log('Time:', Date.now());
  next();
});

app.use('/user/:id', function (req, res, next) {
  console.log('Request Type:', req.method);
  next();
});

app.get('/user/:id', function (req, res, next) {
  res.send('USER');
});


//Second example
app.get('/admin/:id', function (req, res, next) {
  console.log('Admin id:', req.params.id);
  next();
}, function (req, res, next) {
  res.send('Admin Info');
});

// handler for the /user/:id path, which prints the user ID
app.get('/admin/:id', function (req, res, next) {
  res.end(req.params.id);
});


app.get('/superadmin/:id', function (req, res, next) {
  // if the user ID is 0, skip to the next route
  if (req.params.id == 0) next('route');
  // otherwise pass the control to the next middleware function in this stack
  else next(); //
}, function (req, res, next) {
  // render a regular page
  res.render('regular');
});

// handler for the /user/:id path, which renders a special page
app.get('/superadmin/:id', function (req, res, next) {
  res.render('special');
});

app.listen(3000);

