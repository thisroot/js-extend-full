# Задачи
1. Подключите некоторые из представленных внешних middleware библиотек и разберитесь для чего они нужны и попробуйте применить

    Перечень рекомендуемых к изучению:
    - body-parser
    - morgan

    Кроме того особо упоротые могут посмотреть в сторону

    - [passport](http://www.passportjs.org)
    - [socket.io](https://www.npmjs.com/package/socket.io)
    - [nodemailer](https://nodemailer.com/about/)