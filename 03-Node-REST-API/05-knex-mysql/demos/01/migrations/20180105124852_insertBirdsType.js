
exports.up = function(knex, Promise) {
    return Promise.all([
        knex("typeBirds").insert([
            {
                title: "Гагара Белоклювая", 
                description: "Лесная птица живет в средней полосе"
            },
            {
                title: "Качурка северная", 
                description: "Лесная птица живет в Северо-Восточно части"
            },
            {
                title: "Баклан Японский", 
                description: "Живет приемущественно вблизи берегов Японии"
            },
            {
                title: "Нырок Белоглазый", 
                description: "Водоплавающая птица имеет широкий ареал распространения"
            }
        ])
    ]);
};

exports.down = function(knex, Promise) {
   
};
