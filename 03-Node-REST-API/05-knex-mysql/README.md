# Работаем c базами данных посредством  Knex

`Knex` - является `ORM` (Обьектно-реляционным-отображением)  — технология программирования, которая связывает базы данных с концепциями объектно-ориентированных языков программирования, создавая «виртуальную объектную базу данных»

### Имеет широкие возможности, такие как:
- Конструктор моделей
- Миграции (синхронизация структуры моделей и таблиц базы данных)
- Построитель запросов
- Транзакции
- Несколько интерфейсов (промисы, коллбэки, события)

## Установка дистрибьютива
- Инициализируем новое приложение.
- Не забудем установить express
- Установим так же Knex (глобально, чтобы использовать CLI)
- Базу данных (для простоты будем использовать sqlite)

```bash
    npm init -yes && npm install express sqlite3 -S && npm install knex -g
```

- Кроме того можно [установить эту IDE](http://sqlitebrowser.org/) для просмотра структуры и данных в бд

## Настройка приложения

В этом уроке мы создадим простое приложение которое использует базу данных для хранения информации о птицах и месте в котором мы их видели.

- Будет 2 таблицы, 
    - с птицей, ее видом, местом и временем ее наблюдения
    - таблица справочник по видам птиц

- Мы сможем используя RESTAPI заносить данные в таблицы и извлекать их

### Создадим базовый шаблон express приложения

- `index.js`
```js
    var express = require('express');
    var app = express();
    var birds = require('./birds');

    app.use('/birds', birds);

    app.listen(3000, () => console.log('Example app listening on port 3000!'))

```
- `./routes/birds.js`
```js

    var express = require('express');
    var router = express.Router();

    router.get('/', function(req, res) {
        res.send('ok get');
    });

    router.post('/', function(req, res) {
        res.send('ok post');
    });

    module.exports = router;

```

- Создадим файл конфигурации `knexfile.js`

```js
    module.exports = {
        development: {
            client: 'sqlite3',
            connection: {
                filename: "./mydb.sqlite"
            }
        }
    }
```

- Создадим файл `db.js`, в который импортируем настройки и укажем окружение которое будем применять

```js
    var config      = require('./knexfile');
    var env         = 'development';  
    var knex        = require('knex')(config[env]);

    module.exports = knex;

    knex.migrate.latest([config]); 
```

- Так же добавим в наш index.js файл, ссылку на db.js
```js
    var db  = require('./db');
```

- После этого в корневой директории создадим файл миграции

```bash
    knex migrate:make birds
```
- Обратим внимание на то, что у нас автоматически создался каталог `migrations`, в котором создался файл с таймштампом и именем миграции

```js

    exports.up = function(knex, Promise) {

        return Promise.all([
            knex.schema.createTable('birds', function(table) {
                table.increments('uid').primary();
                table.integer('birdTypeId')
                    .references('typeBirdsId')
                    .inTable('typeBirds')
                table.string('location');
                table.timestamps();
            }),

            knex.schema.createTable('typeBirds', function(table){
                table.increments('typeBirdsId').primary();
                table.string('title');
                table.string('description');
            })
        ])
    };

    exports.down = function(knex, Promise) {
         return Promise.all([
            knex.schema.dropTable('birds'),
            knex.schema.dropTable('typeBirds')
        ])
    };

```


- Увидим заготовки под 2 экспортируемые функции
    - Первая функция создания, модификации таблиц
    - Вторая отката назад
- Заполним функции своими схемами
- выполним команду
```bash
     knex migrate:latest
```
- Просто запустим приложение, если ничего не произошло, а наш файл `mydb.sqlite` стал очень большим то нам судя по всему пока везет.

### Добавление данных в справочник

- Создадим еще одну миграцию в которой заполним таблицу видов птиц. 
- Эта таблица является справочником, поэтому доступ через API к ней не предполагается (для простого пользователя)

```bash
    knex migrate:make insertBirdsType
```

```js
    exports.up = function(knex, Promise) {
        return Promise.all([
            knex("typeBirds").insert([
                {
                    title: "Гагара Белоклювая", 
                    description: "Лесная птица живет в средней полосе"
                },
                {
                    title: "Качурка северная", 
                    description: "Лесная птица живет в Северо-Восточно части"
                },
                {
                    title: "Баклан Японский", 
                    description: "Живет приемущественно вблизи берегов Японии"
                },
                {
                    title: "Нырок Белоглазый", 
                    description: "Водоплавающая птица имеет широкий ареал распространения"
                }
            ])
        ]);
    };

    exports.down = function(knex, Promise) {
    
    };
```

- Не забудем обновить базу данных

```bash
     knex migrate:latest
```

- Проверим наличие занесенных данных в бд
![](./img/bd.png)

### Понимание RESTfull API

Для начала разберемся с концепцией RESTfull API
- В протоколе HTTP в заголовках блока сообщений есть описание метода запроса. `GET`, `POST`, `PUT`, `DELETE` ...
- Согласно этому подходу при получении запросов с обозначенным методом, ваш сервер должен выполнять соответствующие этому методу задачи
    - `GET` - получение данных
    - `POST`- добавление новых данных
    - `PUT` - обновление данных
    - `DELETE` - удаление данных 

### Создание контроллеров RESTfull API

- Так как в нашей базе данных есть связанные ключами таблицы, то нам надо знать идентификатор вида птицы, которую мы хотим занести в базу. Для этого создадим контроллер с методом GET для получения справочника видов птиц.


- добавим новый роут в файл birds.js
```js
var db  = require('./db.js');

router.get('/catalog', function(req, res) {
    db('typeBirds').then(rows => {
        res.json(rows);
    })
});

```
- При запросе по адресу `http://127.0.0.1:3000/birds/catalog` мы будем получать json массив справочных данных

- Теперь мы можем создать контроллер для добавления мест в которых мы обнаружили птицу
    - Метод POST предполагает то что клиент передает данные в теле запроса, поэтому нам надо научиться извлекать эти данные
    - Для этого express предлагает использовать промежуточное ПО -  `body-parser`
    - [Установим это промежуточное ПО](https://www.npmjs.com/package/body-parser) 
    ```bash
        npm install body-parser -S
    ``` 
    - Добавим его в нашу программу `index.js`
    ```js
        var bodyParser = require('body-parser');
        //для парсинга json данных c заголовом application/json
        app.use(bodyParser.json());
        // для парсинга данных с заголовком application/x-www-form-urlencoded 
        app.use(bodyParser.urlencoded({ extended: true })); 
    ```
    - Изменим логику нашего контроллера и попробуем получить обратно те данные которые передаем в запросе, файл `birds.js`
    ```js
        router.post('/', function(req, res) {
            res.json(req.body);
        });
    ```
    - Для удобства разработки нашего приложения мы можем установить REST API клиент, в котором мы будем подготавливать запросы отправлять их на сервер и получать ответы
    - Установим программу [Postman](https://www.getpostman.com/)
    - Подготовим тело POST запроса
    ```json
    {
        "birdType": 1,
        "location": "Nizhny Novgorod"
    }

    ```
    ![](./img/postman.png)
    - Отлично, запрос приходит и отправляется обратно.
    - Теперь нам надо обработать и занести эти данные в базу данных
    - Изменим контроллер
    ```js
        router.post('/', function(req, res) {

            if(!req.body.birdType || !req.body.location) {
                res.sendStatus(400);               
            } 
            else {
                let date = new Date().toISOString().slice(0, 19).replace('T', ' ');
                
                const ins = {
                    birdTypeId: req.body.birdType,
                    location: req.body.location,
                    created_at: date,
                    updated_at: date
                }
            
                db('birds').insert(ins).then(rows => {
                    ins.id = rows[0];
                    res.json(ins);
                }).catch(err => {
                    res.json(err);
                })
            }
        });

    ```

    - Если все прошло гладко то мы должны получить соответствующий ответ
    ```json
        {
            "birdTypeId": 1,
            "location": "Nizhny Novgorod",
            "created_at": "2018-01-07 11:56:18",
            "updated_at": "2018-01-07 11:56:18",
            "id": 5
        }
    ```
    ![](./img/postman1.png)

- Сейчас нам надо создать контроллер для получения всех записей, которые мы добавили в нашу Базу данных
    - Изменим контроллер GET запроса
    ```js
        router.get('/', function(req, res) {
            db('birds').then(rows => {
                res.json(rows);
            }).catch(err => {
                res.sendStatus(501);
            })
        });

    ```
    - Если все прошло гладко, то мы получим массив обьектов, которые мы заносили в базу.
    ```json
        [
            {
                "uid": 1,
                "birdTypeId": 1,
                "location": "Nizhny Novgorod",
                "created_at": "2018-01-07 11:48:22",
                "updated_at": "2018-01-07 11:48:22"
            },
            {
                "uid": 2,
                "birdTypeId": 1,
                "location": "Nizhny Novgorod",
                "created_at": "2018-01-07 11:49:49",
                "updated_at": "2018-01-07 11:49:49"
            },
            ...
        ]

    ```

