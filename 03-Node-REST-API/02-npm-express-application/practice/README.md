1. Создайте роутер который будет распознавать формат запроса и однобразно проводить обработку Различные форматы номеров телефонов (распознавание коротких городских и мобильных адресов) 

    - Тест: 
        - 1 группа 235-457-682, 547985354, 4125-5414-4 (9 цифр)
        - 2 группа 8-908-415-5245, 89041246589, +7-912-452-3641 (11 цифр и возможный префикс +)
3. Разделить обработку маршрутов в зависимости от диапазона указываемых лет, а так же обрабатывать маршрут как ошибочный если даты не попадают в указанные диапазоны

    - Тест: 
        - 1 группа 1984-1996
        - 2 группа 1997-2001
        - 3 группа (ошибочная) 1982, 2006 и.т.д

3. Распознать имеется ли в запросе корректное указание времени
    - Тест: 
        - '12:59', '23:41', '00:12', '00:00', '09:15'. Время '24.00', '25.00', '12.60', '12.93', '41.93' является некорректным