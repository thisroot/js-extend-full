// src/people.js
const people = [{
    manager: 'Jen',
    name: 'Ron'
  }, {
    manager: 'Jen',
    name: 'Sue'
  }, {
    manager: 'Hue',
    name: 'Shirley'
  }, {
    manager: 'Bob',
    name: 'Terrence'
  }]
  
  export default people