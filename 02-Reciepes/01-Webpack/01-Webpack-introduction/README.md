# Сборка проекта
- [Babel](http://babeljs.io/) для того чтобы использовать все возможности современного javascript необходимо применить специальную программу которая транспилирует es5 код в старую нотацию.
- [Saas](https://sass-lang.com/) - Вам может понадобиться возможность поддержки препроцессоров стилей.
- [Webpack](https://webpack.js.org/) - помимо всего вышеперечисленного Вам могут понадобиться еще некоторые "фишки"
    - "Склеивание" всех файлов проекта в один бандл, для сокращения времени загрузки сайта
    - Загрузка статических ресурсов, таких как изображения или видеозаписи.
    - Различные возможности длч отладки проекта с применением рабочих окружений
    - "Горячая перезагрузка" - применение и пересборка проекта в режиме разработчика

## Сборщик пакетов Webpack

![](./img/01.png)

Webpack стал одним из самых важных инструментов для современного веб-разработки. В первую очередь это модуль для вашего JavaScript, но его можно научить трансформировать все ваши  файлы проекта, такие как HTML и CSS, даже изображения. Он может дать вам больше контроля над количеством HTTP-запросов, создаваемых вашим приложением, и позволяет использовать другие варианты этих файлов (например, Jade, Sass & ES6). Webpack также позволяет вам легко интегрировать в проект пакеты из npm.

## Установка webpack

- выполним в терминале следующие команды
```bash
    mkdir webpack-demo
    cd webpack-demo
    npm init -y
    npm install webpack@latest --save-dev
    mkdir src
    touch index.html src/app.js webpack.config.js
```

- Отредактируем следуюшие файлы

```html
    <!-- index.html -->
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Hello webpack</title>
    </head>
    <body>
        <div id="root"></div>
        <script src="dist/bundle.js"></script>
    </body>
    </html>
```

```js
    // src/app.js
    const root = document.querySelector('#root')
    root.innerHTML = `<p>Hello webpack.</p>`

```

```js
    // webpack.config.js
    const webpack = require('webpack')
    const path = require('path')

    const config = {
    context: path.resolve(__dirname, 'src'),
    entry: './app.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [{
        test: /\.js$/,
        include: path.resolve(__dirname, 'src'),
        use: [{
            loader: 'babel-loader',
            options: {
            presets: [
                ['es2015', { modules: false }]
            ]
            }
        }]
        }]
    }
    }

    module.exports = config
```
Конфигурация приведенная выше является общей отправной точкой, она инструктирует `webpack` скомпилировать нашу точку входа `src/app.js` в наш выходной файл `/dist/bundle.js`, и все файлы `.js` будут переделаны с `ES2015` на ES5 с помощью `Babel`.

Для этого вам нужно будет установить три пакета, `babel-core`, загрузчик babel-загрузчика `webpack` и предустановленный `babel-preset-es2015` для удобства `JavaScript`, который мы хотим написать. `{modules: false}` позволяет `Tree Shaking` удалять неиспользуемые экспортируемые модули из вашего пакета, чтобы уменьшить размер файла.

```bash
 npm install babel-core babel-loader babel-preset-es2015 --save-dev
```

- Заменим секцию файла package.json своим содержимым

```json
    "scripts": {
    "start": "webpack --watch",
    "build": "webpack -p"
    },
```

Запустим приложение `npm start` из командной строки webpack в режиме просмотра, который будет перекомпилировать наш пакет, когда файл .js будет изменен в нашей директории src. Вывод в консоли сообщает нам о создаваемых связках, важно следить за количеством пакетов и их размером.

- откроем файл index.html и убедимся что все загрузилось как надо.

Откройте `dist/bundle.js,` чтобы узнать, что сделал `webpack`, вверху - код загрузки модуля webpack, а внизу - наш модуль. Теперь вы можете начать создавать модули ES6, и webpack сможет создать комплект для продакшен окружния, который будет работать во всех браузерах.

Остановите `webpack` с помощью Ctrl + C и запустите `npm run build`, чтобы скомпилировать наш пакет в режиме продакшен.

Обратите внимание, что размер пакета уменьшился с 2.61 до 585 байт.
Посмотрите еще раз на `dist/bundle.js`, и вы увидите то что пакет был минимизирован с помощью `UglifyJS`, код будет работать точно так же, но он содержит меньшее количество символов чем его предшественник.

## Модули

- Теперь мы можем испортировать модули по правилам es6

```bash
npm install lodash --save
```

```js
    // src/app.js
    import {groupBy} from 'lodash/collection'

    const people = [{
    manager: 'Jen',
    name: 'Bob'
    }, {
    manager: 'Jen',
    name: 'Sue'
    }, {
    manager: 'Bob',
    name: 'Shirley'
    }, {
    manager: 'Bob',
    name: 'Terrence'
    }]

    const managerGroups = groupBy(people, 'manager')

    const root = document.querySelector('#root')
    root.innerHTML = `<pre>${JSON.stringify(managerGroups, null, 2)}</pre>`

```

- Перенесем содержимое обьекта люди в другой файл

```js

// src/people.js
const people = [{
  manager: 'Jen',
  name: 'Bob'
}, {
  manager: 'Jen',
  name: 'Sue'
}, {
  manager: 'Bob',
  name: 'Shirley'
}, {
  manager: 'Bob',
  name: 'Terrence'
}]

export default people

```

- Теперь наш файл app.js будет таким
```js

// src/app.js
import {groupBy} from 'lodash/collection'
import people from './people'

const managerGroups = groupBy(people, 'manager')

const root = document.querySelector('#root')
root.innerHTML = `<pre>${JSON.stringify(managerGroups, null, 2)}</pre>`


```

Вызовите npm start, чтобы запустить webpack и обновить index.html, вы должны увидеть массив людей, сгруппированных по менеджеру.

- Давайте переместим массив людей в свой собственный модуль `people.js`

## Загрузчики

- Теперь мы можем добавить в наш проект различные загрузчики, позволяющие работать с файлами различных типов.


## Sass
- Трансформация в css файлы
```bash
npm install css-loader style-loader sass-loader node-sass --save-dev
```
- Добавим новое правило в наш конфигурационный файл

```js
    // webpack.config.js
    rules: [{
    test: /\.scss$/,
    use: [
        'style-loader',
        'css-loader',
        'sass-loader'
    ]
    }, {
    // ...
    }]
```
- Теперь чтобы изменения вступили в силу, нам надо перезагрузить наш проект `Ctrl + C` и `npm start`
- Мы получили несколько загрузчиков
    - sass-loader - трансформирует Sass в css
    - css-loader - парсит стили из js файлов и сохраняет зависимости
    - style-loader - выводит наш css в тег `<style>` в документе 

- Вы можете рассматривать их как вызовы функций, выходные данные одного загрузчика загружаются как входные данные в следующий.
    
```js
        styleLoader(cssLoader(sassLoader('source')))
```

- Теперь добавим Sass файл в проект

```scss
    /* src/style.scss */
    $bluegrey: #2B3A42;

    pre {
        padding: 20px;
        background: $bluegrey;
        color: #dedede;
        text-shadow: 0 1px 1px rgba(#000, .5);
    }
```

Теперь мы можем поулчить наш файл стилей непосредственно в проект app.js

```js
    // src/app.js
    import './style.scss'

    // ...
```
- Перезагрузите Ваш файл index.html


## CSS в JS

Мы просто импортировали файл Sass из нашего JavaScript в качестве модуля.

Откройте dist/bundle.js и найдите «pre {». Действительно, наш Sass был скомпилирован в строку CSS и сохранен как модуль внутри нашего пакета. Когда мы импортируем этот модуль в наш JavaScript, вы можете выполнить загрузку стилей-стилей во встроенный тег `<style>`.

- Зачем нам это надо?
    - Компонент JavaScript, который вы можете включить в свой проект, может зависеть от того, как другие файлы будут функционировать должным образом (HTML, CSS, Images, SVG), если все они могут быть объединены вместе, так гораздо проще импортировать и использовать их.
    - Исключение мертвого кода: если компонент JS больше не импортируется вашим кодом, CSS больше не будет импортироваться. Созданный пакет будет содержать только код, который что-то делает.
    - CSS-модули: глобальное пространство имен CSS очень затрудняет уверенность в том, что изменение вашего CSS не будет иметь никаких побочных эффектов. Модули CSS изменяют это, делая по умолчанию CSS по-умолчанию и выставляя уникальные имена классов, которые вы можете ссылаться в своем JavaScript.
    - Сокращение количества HTTP-запросов, объединив/ разделив код с помощью умных способов.

### Изображения

Последним этапом нашего изучения лоадеров станет загрузчик изображений.

в стандартном HTML-документе изображения извлекаются, когда браузер встречает тег <img> или элемент с свойством background-image. С помощью webpack вы можете оптимизировать это в случае небольших изображений, сохранив исходный код изображений в виде строк внутри вашего JavaScript. Делая это, вы предварительно загружаете их, и браузеру не придется брать их с отдельными запросами позже.

```bash
    npm install file-loader url-loader --save-dev
```

```js
    // webpack.config.js
    rules: [{
    test: /\.(png|jpg)$/,
    use: [{
        loader: 'url-loader',
        options: { limit: 10000 } // Convert images < 10k to base64 strings
    }]
    }, {
    // ...
    }]
```

- Давайте получим тестовую картинку для проверки работы нашего загрузчика.

```bash
    curl https://raw.githubusercontent.com/sitepoint-editors/webpack-demo/master/src/code.png --output src/code.png
```

- И добавим в наш проект следующий код.

```js
    import codeURL from './code.png'
    const img = document.createElement('img')
    img.src = codeURL
    img.style.backgroundColor = "#2B3A42"
    img.style.padding = "20px"
    img.width = 32
    document.body.appendChild(img)
```

- Этот код добавляет изображние в атрибут srs
```html
<img src="data:image/png;base64,iVBO..." style="background: #2B3A42; padding: 20px" width="32">

```
- Так же спасибо css-loader изображения можно загружать с помощью функции url()

```scss
    /* src/style.scss */
    pre {
    background: $bluegrey url('code.png') no-repeat center center / 32px 32px;
    }
```