# Starter-kit
Широкое распространение открытых репозиьториев с проектами, привело к тому, что Вам зачастую уже доступны станданртные сборки для проектов практически на любом стеке технологий.
- В этом уроке мы рассмотрим готовый "стартер кит" веб проекта развернутый с помошью веб-пак.

Официальная ссылка на репозиторий: [github.com/ADCI/webpack-starter-kit](https://github.com/ADCI/webpack-starter-kit)

## Обзор проекта

#### Построено на:
- Webpack 3.x

#### Стили:
- Normalize.css
- PostCSS: CSSNext **OR** Sass + autoprefixer
- Sourcemaps

**Заметка** ВЫ можете выбрать только  PostCSS или Sass (по умолчанию Sass):
- измените `STYLES` в переменной в package.json
- удалите ненужный каталог (/src/postcss or /src/scss).
- измените стили импортируемые в  src/app.js

#### Скрипты: 
- Javascript: ES2015+ (Babel env config)
- Sourcemaps

#### Проверка стилитики кода:
- (Стили) Stylelint: стандартная конфигурация (+ `stylelint-scss` для Sass)
- (Скрипты) ESLint: рекомендуемая конфигурация (+ опционально: Airbnb config)

#### Изображение:
- спрайт генератор (spritesmith)
- imagemin

#### Горячая перезагрузка браузера
- BrowserSync
- Webpack dev server

## Базовая струтура проекта

**src/**

#### Входная точка: 
- app.js - основной файл проекта импорирующий остальные файлы

#### PostCSS (postcss/) / Sass (scss/):
- основной файл стилей (main.css / main.scss)
- base/:
  - элементы: стили базовых html элементов
- objects/: OOCSS
- layout/: layouts
- components/: BEM, Atomic Design
- settings/: настройки проекта (переменные,и.т.д.)
- utilities/: mixins
  
#### JavaScript (js/):
- main source file (main.js)
- modules/: folder for javascript modules

#### Изображения (img/):
- img/ - источники изображений
- img/sprite/ - источники изображений для спрайтов

#### Шрифты (fonts/) - источник шрифтов

**dist/**

- css/main.css - выходной css
- js/main.js - выходной js
- assets/ - выходне файлы ассоциированные со сборкой (изображения, шрифты, и.т.д)

## Установка
- Перейдем в удобное для размещения Вашего проекта место
- Запустим терминал и выгрузим проект
```bash
 git clone https://github.com/ADCI/webpack-starter-kit
```
- Установим библиотеки
```bash
npm install
```

## Использование

- `npm run build` - сборка проекта
- `npm run start` - сборка и отслеживание изменений (с помощью BrowserSync)
- `npm run start:webpack-server` - сборка и отслеживание изменений (с помощью Webpack dev server)
- `npm run build:prod` - cборка кода для продакшен

