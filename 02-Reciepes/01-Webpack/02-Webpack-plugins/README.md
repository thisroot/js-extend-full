# Плагины webpack

Мы уже видели один пример встроенного плагина webpack, webpack -p, который вызывается из нашего скрипта сборки npm, использует UglifyJsPlugin, который поставляется с webpack, чтобы минимизировать наш пакет для продакшен.

В то время как загрузчики управляют преобразованиями на отдельных файлах, плагины работают с кусками кода.

## Общий код

Commons-chunk-plugin - это еще один основной плагин, который поставляется с веб-пакетом, который можно использовать для создания отдельного модуля с общим кодом в нескольких точках ввода. До сих пор мы использовали единую точку входа и единый выходной файл. Существует множество сценариев в реальном мире, в которых вы сможете разделить код на несколько файлов ввода и вывода.

Если у вас есть две разные области приложения, которые используют оба модуля, например app.js для открытого приложения и admin.js для области администрирования, вы можете создать для них отдельные точки входа так:

```js
    // webpack.config.js
    const webpack = require('webpack')
    const path = require('path')

    const extractCommons = new webpack.optimize.CommonsChunkPlugin({
    name: 'commons',
    filename: 'commons.js'
    })

    const config = {
    context: path.resolve(__dirname, 'src'),
    entry: {
        app: './app.js',
        admin: './admin.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].bundle.js'
    },
    module: {
        // ...
    },
    plugins: [
        extractCommons
    ]
    }

    module.exports = config

```

Обратите внимание на изменение в output.filename, которое теперь включает [имя], это заменяется именем chunk, поэтому мы можем ожидать два выходных пакета из этой конфигурации: app.bundle.js и admin.bundle.js для наших двух точек входа.

Плагин commonschunk создает третий файл commons.js, который включает в себя общие модули из наших точек входа.

```js
// src/app.js
import './style.scss'
import {groupBy} from 'lodash/collection'
import people from './people'

const managerGroups = groupBy(people, 'manager')

const root = document.querySelector('#root')
root.innerHTML = `<pre>${JSON.stringify(managerGroups, null, 2)}</pre>`


```

```js
// src/admin.js
import people from './people'

const root = document.querySelector('#root')
root.innerHTML = `<p>There are ${people.length} people.</p>`
```

Эти точки входа будут выводить следующие файлы:

- app.bundle.js включает в себя модули стиля и lodash/collection
- admin.bundle.js не включает дополнительные модули
- commons.js включает модуль наших людей

```html
    <!-- index.html -->
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Hello webpack</title>
    </head>
    <body>
        <div id="root"></div>
        <script src="dist/commons.js"></script>
        <script src="dist/app.bundle.js"></script>
    </body>
    </html>
```

```html
    <!-- admin.html -->
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Hello webpack</title>
    </head>
    <body>
        <div id="root"></div>
        <script src="dist/commons.js"></script>
        <script src="dist/admin.bundle.js"></script>
    </body>
    </html>
```

Попробуйте загрузить index.html и admin.html в браузере, чтобы увидеть, как они запускаются с автоматически созданными кусками файлов.

## Извлечение CSS

Другим популярным плагином является модуль extract-text-webpack-plugin, который можно использовать для извлечения модулей в свои выходные файлы.

Ниже мы изменим наше правило .scss, чтобы скомпилировать наш Sass, загрузить CSS, а затем извлечь каждый из его собственного набора CSS, тем самым удалив его из нашего пакета JavaScript.

```bash
    npm install extract-text-webpack-plugin@2.0.0-beta.4 --save-dev
```

```js

// webpack.config.js
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const extractCSS = new ExtractTextPlugin('[name].bundle.css')

const config = {
  // ...
  module: {
    rules: [{
      test: /\.scss$/,
      loader: extractCSS.extract(['css-loader','sass-loader'])
    }, {
      // ...
    }]
  },
  plugins: [
    extractCSS,
    // ...
  ]

  ```
  
Перезагрузите webpack, и вы увидите новый пакет app.bundle.css, к которому можно обращаться напрямую.

```html
    <!-- index.html -->
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Hello webpack</title>
        <link rel="stylesheet" href="dist/app.bundle.css">
    </head>
    <body>
        <div id="root"></div>
        <script src="dist/commons.js"></script>
        <script src="dist/app.bundle.js"></script>
    </body>
    </html>
```

- Обновите страницу чтобы убедиться в том, что наш CSS был перенесен в отдельный файл.

