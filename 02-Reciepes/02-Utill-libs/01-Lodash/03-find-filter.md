# find и filter.

Давайте начнем с метода filter. Когда нам нужно из массива отобрать елементы, которые подходят под какое-то условие - мы используем метод filter. Давайте попробуем.

```js
_.filter([1,2,3,4,5], function (item) {
  return item < 3;
})
```
- Первым аргументом у нас будет массив елементов, а вторым итератор, который будет проходить по каждому елементу. 
- Как мы видим, в результате выполнения мы получаем массив елементом [1,2].

Нужно всегда помнить, что в результате выполнения метода filter мы всегда получаем массив. Если мы не нашли ни одного подходящего елемента - то возвращается пустой массив.

-Чаще же всего фильтр используется с массивом обьектов
```js
_.filter([{id: 1, name: 'foo'}, {id:2, name: 'bar'}], function (item) {
  return item.name === 'foo';
});
```
- В результате выполнения мы получили отфильтрованый массив.

Ну и естественно фильтр можно применять к обьектам точно также как и к массивам. Если у нас есть переменная а, то мы можем отфильтровать ее поля.

```js
a = {id:1, name:2}
_.filter(a,function (item){
  return item==1;
})
```

В результате получим массив с елементом 1. Мы проходим по всем значениям обьекта, отбираем все, которые подходят по условию и выводим в массив. Конечно этот пример не особо полезный, но логику работы с обьектом вы поняли.

- Теперь о find. Он работает абсолютно так же как и фильтр, но возвращает не массив, а первый найденный по условию елемент.

```js
_.find([{id: 1, name: 'foo'}, {id:2, name: 'bar'}], function (item){
  return item.name === 'bar';
});
```

В результате как мы видим мы получили обьект. И это был первый, попавший под условие елемент. Обычно это используется, когда елементы уникальны по какому-то ключу. Если бы мы елемент не нашли, то нам бы вернулся undefined. В остальном же метод работает точно так же как и find.

- Теперь давайте рассмотрим как написать меньше кода. Мы можем использовать короткую запись с вторым аргументом в виде обьекта

```js
_.find([{id: 1, name: 'foo'}, {id:2, name: 'bar'}], {id: 1});
```

- Запись получилась намного короче, а результат тот же самый.

- Точно так же можно применять короткую запись с методом filter.

```js
_.filter([{id: 1, name: 'foo'}, {id:2, name: 'bar'}], {id: 1});
```
И на последок - я очень часто встречал в коде когда люди не знают что существует метод find и пишут метод фильтр и вытаскивают с него первый найденный елемент. Это конечно рабочий, но не самый красивый вариант.