
console.log("Callback example");
console.log("=============================");

    console.log( "a" );
    setTimeout(function() {
        console.log( "e" )
    }, 500 );

    setTimeout(function() {
        console.log( "d" )
    }, 200 );

    setTimeout(function() {
        console.log( "c" )
    }, 100 );

    console.log( "b" );

//из за первого примера придется сделать задержку
setTimeout(function(){
    
    console.log("\nPromise example");
    console.log("============================");


    const fetchProducts = () => Promise.resolve({data: [1,2,3]})

    const getProducts = () => {
    fetchProducts().then(data => {
        console.log('data:', data)
    })
    }

    getProducts();

    


    // Комплексный пример промиса
    let promise = new Promise((resolve, reject) => {
        
            setTimeout(() => {
                let a = 4, b = 5;
                // переведёт промис в состояние fulfilled с результатом "result"
                resolve(a+b);
            }, 1000);
    });
        
    // promise.then навешивает обработчики на успешный результат или ошибку
    promise
    .then(
        result => {
        // первая функция-обработчик - запустится при вызове resolve
        console.log("Fulfilled: " + result); // result - аргумент resolve
        },
        error => {
        // вторая функция - запустится при вызове reject
        console.error("Rejected: " + error); // error - аргумент reject
        }
    );

    
    {
        //Цепочка помисов
        let promise = new Promise(function(resolve, reject) {
            setTimeout(() => resolve(1), 1000);
        });
        
        promise.then(function(result) {
            console.log("\nЦепочки промисов")
            console.log("=============================")
            
            console.log(result); // 1
            return result * 2;
        }).then(function(result) {
            console.log(result); // 2
            return result * 2;
        }).then(function(result) {
            console.log(result); // 4
            return result * 2;
        });
    } {
        //Промисификация
        function GetNameFromTimeout(name, timeout) {
            
                return new Promise((resolve, reject) => {
            
                setTimeout(() => {
                    // переведёт промис в состояние fulfilled с результатом "result"
                    resolve(name);
                }, timeout);
            
                });
            }
            
        GetNameFromTimeout("Vasya",4000).then((data) => {
            console.log("\nПромисификация")
            console.log("=============================")
            
            console.log(data);
        });
    } {

        //обработка ошибок
        function GetNameFromTimeout(name, timeout) {
            
                return new Promise((resolve, reject) => {
            
                setTimeout(() => {
                    //вызовем принудительно ошибку
                    reject(
                        //new Error("Произвошла ошибка")
                        "Ошибка произошла"
                    )
                }, timeout);
            
                });
            }
            
            GetNameFromTimeout("Vasya",5000)
            .then((data) => {
                console.log(data);
            })
            //обработаем через стандартный перехватчик ошибок
            .catch((error) => {
                console.log("\nОбработка ошибок");
                console.log("=============================");
               
                console.log(error);
            });
    }

    {
        //Async / await
        async function fetchProducts() {
            if(true) return [1,2,3]
            else throw new Error("Ошибка");
        }
                
        const getProducts = async() => {
            try {
                var result = await fetchProducts()
                // setTimeout(() => {
                //      //можем в синхронном стиле использовать здесь
                //     console.log(result); //[ 1, 2, 3 ]
                // }, 6000);
                return result;
            } 
            catch(err) {
                return err
            }
        }
        
        getProducts()
            .then((data)=> { setTimeout(() => {
                    //можем в синхронном стиле использовать здесь
                    console.log("\nAsync/Await");
                    console.log("=============================");
                    console.log("Сумма =  " + data.reduce((a, b) =>  a + b)); //[ 1, 2, 3 ]
               }, 6500) }
            )
            .catch((err)=> console.log(err));
            
            getProducts();
    }
    
},500);