
//Map
let map = new Map();

map.set('1', 'str1');   // ключ-строка
map.set(1, 'num1');     // число
map.set(true, 'bool1'); // булевое значение

// в обычном объекте это было бы одно и то же,
// map сохраняет тип ключа
console.log( map.get(1)   ); // 'num1'
console.log( map.get('1') ); // 'str1'

console.log( map.size ); // 3
console.log("\n============================\n");

let map2 = new Map();
map2
.set('1', 'str1')
.set(1, 'num1')
.set(true, 'bool1');

console.log(map2);
console.log("\n============================\n");

let map3 = new Map([
    ['1',  'str1'],
    [1,    'num1'],
    [true, 'bool1']
]);

console.log(map3);
console.log("\n============================\n");


let user = { name: "Вася" };
// для каждого пользователя будем хранить количество посещений
let visitsCountMap = new Map();
// объект user является ключом в visitsCountMap
visitsCountMap.set(user, 123);
console.log( visitsCountMap.get(user) ); // 123
console.log("\n============================\n");

let recipeMap = new Map([
    ['огурцов',   '500 гр'],
    ['помидоров', '350 гр'],
    ['сметаны',   '50 гр']
]);

// цикл по ключам
for(let fruit of recipeMap.keys()) {
    console.log(fruit); // огурцов, помидоров, сметаны
}

// цикл по значениям [ключ,значение]
for(let amount of recipeMap.values()) {
    console.log(amount); // 500 гр, 350 гр, 50 гр
}

// цикл по записям
for(let entry of recipeMap) { // то же что и recipeMap.entries()
    console.log(entry); // огурцов,500 гр , и т.д., массивы по 2 значения
}

recipeMap.forEach( (value, key, map) => {
    console.log(`${key}: ${value}`); // огурцов: 500 гр, и т.д.
});

console.log("\n============================\n");

//получение 2-мерного массива
console.log([ ...recipeMap.entries() ]);
//или Array.from(recipeMap)
//[ [ 'огурцов', '500 гр' ],
//  [ 'помидоров', '350 гр' ],
//  [ 'сметаны', '50 гр' ] ]

//получение массива значений
console.log([ ...recipeMap.values() ]);
//[ '500 гр', '350 гр', '50 гр' ]

//получение ключей
console.log([ ...recipeMap.keys() ]);
//[ 'огурцов', 'помидоров', 'сметаны' ]
console.log("\n============================\n");
 
 //Set
let set = new Set();
 
let vasya = {name: "Вася"};
let petya = {name: "Петя"};
let dasha = {name: "Даша"};

// посещения, некоторые пользователи заходят много раз
set.add(vasya);
set.add(petya);
set.add(dasha);
set.add(vasya);
set.add(petya);

// set сохраняет только уникальные значения
console.log( set.size ); // 3

set.forEach( user => console.log(user.name ) ); // Вася, Петя, Даша
console.log("\n============================\n");

//WeakMap, WeakSet
let activeUsers = [
        {name: "Вася"},
        {name: "Петя"},
        {name: "Маша"}
];

// вспомогательная информация о них,
// которая напрямую не входит в объект юзера,
// и потому хранится отдельно
let weakMap = new WeakMap();

weakMap.set(activeUsers[0], 1);
weakMap.set(activeUsers[1], 2);
weakMap.set(activeUsers[2], 3);
try {
    weakMap.set('Katya', 4); //Будет ошибка TypeError: "Katya" is not a non-null object
} 

catch(err) {
    console.log(err);
}

console.log( weakMap.get(activeUsers[0]) ); // 1

activeUsers.splice(0, 1); // Вася более не активный пользователь
// weakMap теперь содержит только 2 элемента
activeUsers.splice(0, 1); // Петя более не активный пользователь